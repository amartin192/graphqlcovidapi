# Express GraphQL Covid19 Api 

## Prerequisites (Developed in)
 - NPM 6.70
 - Node 11.12.0

## Dependencies
 - express
 - express-graphql
 - graphql
 - node-fetch
 - bluebird

## How to Setup
- Setup node and npm
- Install ```yarn``` or run ```npm install``` code directory
- Run using ```yarn dev``` or ```yarn start```
- Runs on port ```4000```

## Example Call


  THe code uses a simple express app and graphql. You can access the graphql UI running at the following ```http://localhost:4000/covidDataUI```. If you want to get an example of data you can access ```http://localhost:4000/covidSummary```

  If using the UI url, it will take you to a UI interpreter of GraphQL you can enter the example json below to pull the data. AllCountriesSummaries can take a parameter that filters on a countries name. The other call just pulls example data of a hard coded schema.
```
{
  getCovidData {
  	Summary {
      NewConfirmed
      TotalRecovered
    },
    AllCountriesSummaries{
      Country
      TotalConfirmed,
      TotalDeaths
    },
    UnitedStates:AllCountriesSummaries(countryName:"United States"){
      Country
      TotalConfirmed,
      TotalDeaths
    }
  }
}
```