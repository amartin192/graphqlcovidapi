const fetch = require('node-fetch');
fetch.Promise = require('bluebird');
const { covidApiUrl } = require('config');

class Covid{
  Summary(){
    console.log('Calling Covid19 Summary');
    return fetch(`${covidApiUrl}/summary`)
      .then(res => res.json())
      .then((results) => {
        let { NewConfirmed, TotalConfirmed, NewDeaths, TotalDeaths, NewRecovered, TotalRecovered} = results.Global;
        return {
          NewConfirmed, 
          TotalConfirmed, 
          NewDeaths, 
          TotalDeaths,
          NewRecovered, 
          TotalRecovered
        }
    });
  }
  
  AllCountriesSummaries( {countryName}){
    console.log('Getting all field summaries')
    return fetch(`${covidApiUrl}/summary`)
      .then(res => res.json())
      .then((results) => {
        let listOfCountries = results.Countries;
       
        return listOfCountries
          .filter((item) => !countryName || item.Country.indexOf(countryName) >= 0)
          .map( countryData => {
              let { NewConfirmed, 
                TotalConfirmed, 
                NewDeaths, 
                TotalDeaths,
                NewRecovered, 
                TotalRecovered,
                Country } = countryData;
                return {
                  Country,
                  NewConfirmed, 
                  TotalConfirmed, 
                  NewDeaths, 
                  TotalDeaths,
                  NewRecovered, 
                  TotalRecovered,
        
                }
          })
        
    });
  }
}



module.exports = Covid;