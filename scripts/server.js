const express = require('express');
const fs = require('fs');
const path = require('path');

const app = express();
const config = require('config');

const apiFolderLocation = path.join(__dirname, ...config.apiFolder.split('/'))

fs.readdir(apiFolderLocation, (err, files) => {
  console.log(apiFolderLocation);
  if(err){
    console.error("The api folder does not exist, please create files in folder that takes the app, configuration parameters");
    process.exit(1);
  }

  files.forEach((file, index) => {
    console.log('Setting up api file', file);
    let api = require(`${config.apiFolder}/${file.replace('.js','')}`);
    api(app);
  })

  app.listen(4000);
})
