var graphqlHTTP = require('express-graphql');
var { parse, buildSchema, graphql, GraphQLObjectType, GraphQLSchema, GraphQLInt } = require('graphql');
let Bluebird = require('bluebird');
let fetch = require('node-fetch');
fetch.Promise = Bluebird;

const Covid = require('../objectTypes/Covid');

let schema = buildSchema(`
 type SummaryData {
  NewConfirmed : Int
  TotalConfirmed : Int 
  NewDeaths : Int
  TotalDeaths : Int
  NewRecovered : Int
  TotalRecovered : Int
  Country: String!
 }
 
 type Covid {
   Summary: SummaryData
   AllCountriesSummaries(countryName: String): [SummaryData]
 }

  type Query {
    getCovidData : Covid
  }
`)

let root = {
  getCovidData: () => {
    return new Covid();
  }
}

let parseContent = (req,res) => {
  return graphql(schema, '{ getCovidData { Summary { NewConfirmed TotalRecovered } } }', root).then(response =>{
    console.log(response);
    res.json(response)
  });
}

module.exports = (api) => {
  api.use('/covidDataUi', graphqlHTTP({
    schema: schema, 
    rootValue: root, 
    graphiql:true
  }))

  api.get('/covidSummary', parseContent )
}